<div class="container-fluid" style="background-color:#E5E5E5;">
        <div class="row">
            <div class="col-lg-6"><h3>Dashboard</h3></div>
            <div class="col-lg-6 text-right pt-3">              
                <a class="btn btn-light mr-0"  data-toggle="dropdown">Last 30 day reports<i class="fa fa-chevron-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                       <li class="pl-3"><a href="#contains">First 5 days</a></li>
                       <li class="pl-3"><a href="#its_equal">First 10 days</a></li>
                       <li class="pl-3"><a href="#greather_than">Within 15 days</a></li>
                       <li class="pl-3"><a href="#less_than">last 20 days</a></li>
                       <li class="pl-3"><a href="#all">25 days ago</a></li>
                    </ul>                   
                </div>
            </div>

            <div class="card-deck mb-5"  style="background-color:#E5E5E5;">
                <div class="card text-center">
                    <div class="card-text"><i class=" iconp fa fa-slideshare"></i></div>
                    <div class="card-body"><h4>Visitors</h4></div>
                    <div class="card-body"><span class="font-weight-bold">125</span><span class="text-danger font-weight-bold">(+ 0.5%)</span></div>
                </div>
                <div class="card text-center">
                    <div class="card-text"><i class="iconp fa fa-download"></i></div>
                    <div class="card-body"><h4>Downloads</h4></div>
                    <div class="card-body">28K</div>
                </div>
                <div class="card text-center">
                    <div class="card-text"><i class="iconp fa fa-bar-chart"></i></div>
                    <div class="card-body"><h4>Projects</h4></div>
                    <div class="card-body">25K</div>
                </div>
                <div class="card text-center">
                    <div class="card-text"><i class=" iconp fa fa-user-md"></i></div>
                    <div class="card-body"><h4>Members</h4></div>
                    <div class="card-body">12K</div>
                </div>
                
            </div>
                </div>


            <div class="mt-4 text-dark font-weight-bold" style="border-bottom: 2px solid #E5E4E2;"><h3>Top Downloads</h3></div>
            <div class="card-deck mt-3">
                <div class="card">
                    <img src="admin/assets/img/Profile-page.png">
                </div>
                <div class="card">
                    <img src="admin/assets/img/Profile-page.png">
                </div>
                <div class="card">
                    <img src="admin/assets/img/Profile-page.png">
                </div>
                <div class="card">
<img src="admin/assets/img/Profile-page.png">                </div>
                <div class="card">
                   <img src="admin/assets/img/Profile-page.png">
                </div>
            </div>
            <div class="card-deck mt-5">
                <div class="card">
                   <img src="admin/assets/img/Profile-page.png">
                </div>
                <div class="card">
                    <img src="admin/assets/img/Profile-page.png">
                </div>
                <div class="card">
<img src="admin/assets/img/Profile-page.png">                </div>
                <div class="card">
                    <img src="img/Profile-page.png">
                </div>
                <div class="card">
                   <img src="admin/assets/img/Profile-page.png">
                </div>
            </div>
<!-- #############________Right-side_End__________############# -->
    </div>
  </div>
</div>
