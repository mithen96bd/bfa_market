<?php 
include_once 'admin/authectication/class.user.php';
$user = new User();
// Checking for user logged in or not
    /*if (!$user->get_session())
    {
       header("location:index.php");
    }*/
if (isset($_POST['submit'])){
        extract($_POST);
        $register = $user->reg_user($fullname, $uname, $upass, $uemail,$zipcode);
        if ($register) {
            // Registration Success
            echo "<div style='text-align:center'>Registration successful <a href='login.php'>Click here</a> to login</div>";
        } else {
            // Registration Failed
            echo "<div style='text-align:center'>Registration failed. Email or Username already exits please try again.</div>";
        }
    }
?>
 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta charset="UTF-8">
  <title>BFA || Signup</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="admin/assets/style.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
      <div class="container">
         <h3 class="py-3 ml-md-5"><span style="color: #A4D268;">BFA</span><span>MARKET</span></h3>
        <div class="signup-form col-md-4 offset-md-4 mt-5">
            <h1 class="tittle mb-3" style="color: #A4D268; text-align: center;">Sign Up</h1>
        <form action="" method="post" name="reg">
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <input type="text" class="form-control"  name="fullname" placeholder="Name">
                    </div>
                   <div class="form-group">
                    <input type="username" class="form-control"  name="uname" " placeholder="User name">
                  </div>

                  <div class="form-group">
                    <input type="email" class="form-control"  value="" name="uemail"  placeholder="Email">
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <input type="password" class="form-control" value="" name="upass" placeholder="New Password">
                    </div>
                    
                  <!-- <div class="form-row">
                    <div class="form-group col-md-8">
                      <select id="inputState" class="form-control">
                        <option>Select Your City...</option>
                        <option>Dhaka</option>
                        <option>Mymensing</option>
                        <option>khulna</option>
                        <option>Sherpur</option>
                        <option>Chittagong</option>
                        <option>Sylhet</option>
                      </select>
                    </div> -->
                    <div class="form-group col-md-4">
                      <input type="text" class="form-control"  value="" name="zipcode" placeholder="Zip Code">
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <div class="form-check ml-5">
                      <input class="form-check-input" type="checkbox" id="gridCheck">
                      <label class="form-check-label" for="gridCheck" style="color: #A4D268;">I agree to terms and condition</label>
                    </div>
                  </div> -->
                  
                  <button type="submit"style="background-color:#A4D268;" value="Register"  class="btn btn-block btn-dark mb-3 border-0" name="submit" onclick="return(submitreg());"  >Submit</button>
                 
                </form>
                <h4 class="m-0" style="color: #A4D268">or</h4>  
            <ul class="ml-0">
                <li ><a style="color: #3b5998;" href="http://www.facebook.com/page.bfa"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a style="color: #db4437" href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a style="color: #f46f30;" href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a style="color:#1da1f2;" href="http://www.twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a style="color: #007bb5;" href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li> 
                <li><a style="color: #ff0000;" href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li> 
            </ul>         
        </div>
      </div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script type="text/javascript" src="admin/assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="admin/assets/js/script.js"></script>
</body>
</html>
    <script>
      function submitreg() {
        var form = document.reg;
        if (form.name.value == "") {
          alert("Enter name.");
          return false;
        } else if (form.uname.value == "") {
          alert("Enter username.");
          return false;
        } else if (form.upass.value == "") {
          alert("Enter password.");
          return false;
        } else if (form.uemail.value == "") {
          alert("Enter email.");
          return false;
        }
        else if (form.zipcode.value == "") {
          alert("zipcode.");
          return false;
        
        }
      }
      
    </script>
  </body>

  </html>