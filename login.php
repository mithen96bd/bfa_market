<?php 
session_start();
include_once 'admin/authectication/class.user.php';
$user = new User();

if (isset($_POST['submit'])) { 
		extract($_POST);   
	    $login = $user->check_login($emailusername, $password);
	    if ($login) {
	        // Registration Success
	       header("location:home.php");
	    } else {
	        // Registration Failed
	        echo 'Wrong username or password';
	    }
	}
?>
  <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta charset="UTF-8">
  <title>BFA|Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="/text/css" href="admin/assets/style.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
      <div class="container">
        <h3 class="py-3 ml-md-5"><span style="color: #A4D268;">BFA</span><span>MARKET</span></h3>
    <div class="login-form col-md-4 offset-md-4 mt-5">
      <form action="" method="post" name="login">

        <h1 class="tittle mb-3" style="color: #A4D268; text-align: center;">Log In</h1>
          <input type="uname" name="emailusername" class="form-control mb-3" placeholder="username">

          <input type="password"  class="form-control mb-3" name="password" placeholder="Password">

          <button type="submit" class="btn btn-success btn-block" name="submit" onclick="return(submitlogin());">Sign in</button>
          <div class="py-3 mb-0">
            <a class="form-link" href="#" style="color: #A4D268">Forgot password?</a>
            <span class="ml-5">No account?<a class="" href="registration.php" style="color: #A4D268">Sign up</a></span>
          </div>
          <h4 style="color: #A4D268">or</h4>  
            <ul class="ml-0">
                <li ><a style="color: #3b5998;" href="http://www.facebook.com/page.bfa"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a style="color: #db4437" href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a style="color: #f46f30;" href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a style="color:#1da1f2;" href="http://www.twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a style="color: #007bb5;" href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li> 
                <li><a style="color: #ff0000;" href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li> 
            </ul> 
        </form> 
        </div>
      </div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script type="text/javascript" src="admin/assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="admin/assets/js/script.js"></script>
</body>
</html>
    <script>
      function submitlogin() {
        var form = document.login;
        if (form.emailusername.value == "") {
          alert("Enter email or username.");
          return false;
        } else if (form.password.value == "") {
          alert("Enter password.");
          return false;
        }
      }
    </script>


  </body>